package sandoval.daniel.dao;

import sandoval.daniel.bl.entities.Administrador.IAdministradorDAO;
import sandoval.daniel.bl.entities.Administrador.MySqlAdministradorImpl;
import sandoval.daniel.bl.entities.Auto.IAutoDAO;
import sandoval.daniel.bl.entities.Auto.MySqlAutoImpl;
import sandoval.daniel.bl.entities.Canton.ICantonDAO;
import sandoval.daniel.bl.entities.Canton.MySqlCantonImpl;
import sandoval.daniel.bl.entities.Conductor.IConductorDAO;
import sandoval.daniel.bl.entities.Conductor.MySqlConductorImpl;
import sandoval.daniel.bl.entities.Direccion.IDireccionDAO;
import sandoval.daniel.bl.entities.Direccion.MySqlDireccionImpl;
import sandoval.daniel.bl.entities.Distrito.IDistritoDAO;
import sandoval.daniel.bl.entities.Distrito.MySqlDistritoImpl;
import sandoval.daniel.bl.entities.Moto.IMotoDAO;
import sandoval.daniel.bl.entities.Moto.MySqlMotoImpl;
import sandoval.daniel.bl.entities.Pago.IPagoDAO;
import sandoval.daniel.bl.entities.Pago.MySqlPagoImpl;
import sandoval.daniel.bl.entities.Pedido.IPedidoDAO;
import sandoval.daniel.bl.entities.Pedido.MySqlPedidoImpl;
import sandoval.daniel.bl.entities.Persona.IPersonaDAO;
import sandoval.daniel.bl.entities.Persona.MySqlPersonaImpl;
import sandoval.daniel.bl.entities.Provincia.IProvinciaDAO;
import sandoval.daniel.bl.entities.Provincia.MySqlProvinciaImpl;
import sandoval.daniel.bl.entities.Servicio.IServicioDAO;
import sandoval.daniel.bl.entities.Servicio.MySqlServicioImpl;
import sandoval.daniel.bl.entities.Transporte.ITransporteDAO;
import sandoval.daniel.bl.entities.Transporte.MySqlTransporteImpl;
import sandoval.daniel.bl.entities.Usuario.IUsuarioDAO;
import sandoval.daniel.bl.entities.Usuario.MySqlUsuarioImpl;

public class MySqlDaoFactory extends DaoFactory{

    public IAdministradorDAO getIAdministradorDao() {
        return new MySqlAdministradorImpl();
    }

    public IAutoDAO getIAutoDao() {
        return new MySqlAutoImpl();
    }

    public ICantonDAO getICantonDao() {
        return new MySqlCantonImpl();
    }

    public IConductorDAO getIConductorDao() {
        return new MySqlConductorImpl();
    }

    public IDireccionDAO getIDireccionDao() {return new MySqlDireccionImpl();}

    public IDistritoDAO getIDistritoDao() {
        return new MySqlDistritoImpl();
    }

    public IMotoDAO getIMotoDao() {
        return new MySqlMotoImpl();
    }

    public IPagoDAO getIPagoDao() {
        return new MySqlPagoImpl();
    }

    public IPedidoDAO getIPedidoDao() {return new MySqlPedidoImpl();}

    public IPersonaDAO getIPersonaDao() {return new MySqlPersonaImpl();}

    public IProvinciaDAO getIProvinciaDao() {return new MySqlProvinciaImpl();}

    public IServicioDAO getIServicioDao() {return new MySqlServicioImpl();}

    public ITransporteDAO getITransporteDao() {return new MySqlTransporteImpl();}

    public IUsuarioDAO getIUsuarioDao() {return new MySqlUsuarioImpl();}

}
