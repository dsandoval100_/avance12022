package sandoval.daniel.dao;


import sandoval.daniel.bl.entities.Administrador.IAdministradorDAO;
import sandoval.daniel.bl.entities.Auto.IAutoDAO;
import sandoval.daniel.bl.entities.Canton.ICantonDAO;
import sandoval.daniel.bl.entities.Conductor.IConductorDAO;
import sandoval.daniel.bl.entities.Direccion.IDireccionDAO;
import sandoval.daniel.bl.entities.Distrito.IDistritoDAO;
import sandoval.daniel.bl.entities.Moto.IMotoDAO;
import sandoval.daniel.bl.entities.Pago.IPagoDAO;
import sandoval.daniel.bl.entities.Pedido.IPedidoDAO;
import sandoval.daniel.bl.entities.Persona.IPersonaDAO;
import sandoval.daniel.bl.entities.Provincia.IProvinciaDAO;
import sandoval.daniel.bl.entities.Servicio.IServicioDAO;
import sandoval.daniel.bl.entities.Transporte.ITransporteDAO;
import sandoval.daniel.bl.entities.Usuario.IUsuarioDAO;
import sandoval.daniel.util.Utils;

public abstract class DaoFactory {

    public static DaoFactory getDaoFactory() {
        String repository;
        try {
            repository = Utils.getProperties()[6];
            switch (repository){
                case "MYSQL": return new MySqlDaoFactory();
                default: return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public abstract IAdministradorDAO getIAdministradorDao();
    public abstract IAutoDAO getIAutoDao();
    public abstract ICantonDAO getICantonDao();
    public abstract IConductorDAO getIConductorDao();
    public abstract IDireccionDAO getIDireccionDao();
    public abstract IDistritoDAO getIDistritoDao();
    public abstract IMotoDAO getIMotoDao();
    public abstract IPagoDAO getIPagoDao();
    public abstract IPedidoDAO getIPedidoDao();
    public abstract IPersonaDAO getIPersonaDao();
    public abstract IProvinciaDAO getIProvinciaDao();
    public abstract IServicioDAO getIServicioDao();
    public abstract ITransporteDAO getITransporteDao();
    public abstract IUsuarioDAO getIUsuarioDao();





}
