package sandoval.daniel.bl.entities.Moto;

import sandoval.daniel.bl.entities.Transporte.Transporte;

/**
 * @author Daniel Sandoval
 * @version 2.0
 * @since 4-19-2022
 *
 *Esta clase se encarga de gestionar todos los objetos moto .
 *
 * */

public class Moto extends Transporte {

    /**
     * se genara el constructor vacio
     * **/
    public Moto() {
    }
    /**
     * se genara el constructor con los parametros
     * **/
    public Moto(String marca, String placa, String tipo) {
        super(marca, placa, tipo);
    }
    /**
     * se genara el to string
     * **/
    @Override
    public String toString() {
        return super.toString() + "Moto{}";
    }
    /**
     * se genara el metedo equals
     * **/
    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }
}
