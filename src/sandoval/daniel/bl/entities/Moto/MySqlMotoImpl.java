package sandoval.daniel.bl.entities.Moto;

import sandoval.daniel.dl.bd.Conector;

import java.sql.ResultSet;
import java.util.ArrayList;

public class MySqlMotoImpl implements IMotoDAO {

    private String sql="";




    public ArrayList<Moto> listarMoto() throws Exception {
        ArrayList<Moto> listaMotos = new ArrayList<>();
        sql="SELECT * FROM MOTO";
        ResultSet rs = Conector.getConnector().ejecutarQuery(sql);

        while (rs.next()){
            Moto moto = new Moto(rs.getString("MARCA"),rs.getString("PLACA"),rs.getString("TIPO"));
            listaMotos.add(moto);
        }
        return listaMotos;
    }

    public Moto buscarMoto(Moto moto) throws Exception {
        sql ="SELECT MARCA, PLACA, TIPO" +
                "FROM MOTO " +
                "WHERE PLACA='"+moto.getPlaca()+"'";
        ResultSet rs = Conector.getConnector().ejecutarQuery(sql);

        if(rs.next()){
            Moto motoEncontrado = new Moto(rs.getString("MARCA"),rs.getString("PLACA"),rs.getString("TIPO"));
            return motoEncontrado;
        }
        return null;
    }


    public String registrarMoto(Moto moto) throws Exception {
        sql = "INSERT INTO MOTO VALUES ('"+moto.getMarca()+"','"+moto.getPlaca()+"','"+moto.getTipo()+"')";
        Conector.getConnector().ejecutarSQL(sql);

        return "Se registro el MOTO en MySQL";
    }



    public String actualizarMoto(Moto moto) throws Exception {
        sql = "UPDATE MOTO SET MARCA = '"+moto.getMarca()+"',PLACA = '"+moto.getPlaca()+"',TIPO = '"+moto.getTipo()+"' " +
                "WHERE PLACA = '"+moto.getPlaca()+"'";
        Conector.getConnector().ejecutarSQL(sql);

        return "Se actualizó el MOTO en MySQL";
    }

    public String eliminarMoto(Moto moto) throws Exception {
        sql = "DELETE FROM MOTO WHERE PLACA='"+moto.getPlaca()+"'";
        Conector.getConnector().ejecutarSQL(sql);

        return "Se eliminó el MOTO en MySQL";
    }


}
