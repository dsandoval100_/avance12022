package sandoval.daniel.bl.entities.Moto;

import sandoval.daniel.bl.entities.Administrador.Administrador;

import java.util.ArrayList;

public interface IMotoDAO {
    String registrarMoto(Moto moto) throws Exception;
    String actualizarMoto(Moto moto) throws Exception;
    String eliminarMoto(Moto moto) throws Exception;
    ArrayList<Moto> listarMoto() throws Exception;
    Moto buscarMoto(Moto moto) throws Exception;

}
