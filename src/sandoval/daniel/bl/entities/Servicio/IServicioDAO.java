package sandoval.daniel.bl.entities.Servicio;


import sandoval.daniel.bl.entities.Usuario.Usuario;

import java.util.ArrayList;

public interface IServicioDAO {
    String registrarServicio(Servicio servicio) throws Exception;
    String actualizarServicio(Servicio servicio) throws Exception;
    String eliminarServicio(Servicio servicio) throws Exception;
    ArrayList<Servicio> listarServicio() throws Exception;
    Servicio buscarServicio(Servicio servicio) throws Exception;

}
