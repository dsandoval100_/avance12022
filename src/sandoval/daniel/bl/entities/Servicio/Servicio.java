package sandoval.daniel.bl.entities.Servicio;

import sandoval.daniel.bl.entities.Direccion.Direccion;

import java.util.Objects;

/**
 * @author Daniel Sandoval
 * @version 2.0
 * @since 4-19-2022
 *
 *Esta clase se encarga de gestionar todos los objetos servicio .
 *
 * */

public class Servicio {
    /**
     * se genara los atributos
     * **/
    private int codigo;
    private String descripcion;
    private String precio;
    private String tipo;
    private String estado;
    private Direccion direccionn;

    /**
     * se genara el constructor vacio
     * **/
    public Servicio() {
    }
    /**
     * se genara el constructor con los parametros
     * **/
    public Servicio(int codigo, String descripcion, String precio, String tipo, String estado) {
        this.codigo = codigo;
        this.descripcion = descripcion;
        this.precio = precio;
        this.tipo = tipo;
        this.estado = estado;
    }
    /**
     * se genaran los getters y setters
     * **/
    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Direccion getDireccionn() {
        return direccionn;
    }

    public void setDireccionn(Direccion direccionn) {
        this.direccionn = direccionn;
    }

    /**
     * se genara el to string
     * **/
    @Override
    public String toString() {
        return "Servicio{" +
                "codigo=" + codigo +
                ", descripcion='" + descripcion + '\'' +
                ", precio='" + precio + '\'' +
                ", tipo='" + tipo + '\'' +
                ", estado='" + estado + '\'' +
                ", direccionn=" + direccionn +
                '}';
    }

    /**
     * se genara el metedo equals
     * **/
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Servicio servicio = (Servicio) o;
        return codigo == servicio.codigo && Objects.equals(descripcion, servicio.descripcion) && Objects.equals(precio, servicio.precio) && Objects.equals(tipo, servicio.tipo) && Objects.equals(estado, servicio.estado) && Objects.equals(direccionn, servicio.direccionn);
    }


}
