package sandoval.daniel.bl.entities.Servicio;

import sandoval.daniel.bl.entities.Moto.Moto;
import sandoval.daniel.bl.entities.Usuario.Usuario;
import sandoval.daniel.dl.bd.Conector;

import java.sql.ResultSet;
import java.util.ArrayList;

public class MySqlServicioImpl implements IServicioDAO{

    private String sql="";

    public ArrayList<Servicio> listarServicio() throws Exception {
        ArrayList<Servicio> listaServicios = new ArrayList<>();
        sql="SELECT CODIGO,DESCRIPCION,PRECIO,TIPO,ESTADO FROM SERVICIO";
        ResultSet rs = Conector.getConnector().ejecutarQuery(sql);

        while (rs.next()){
            Servicio servicio = new Servicio(rs.getInt("CODIGO"),rs.getString("DESCRIPCION"),
                    rs.getString("PRECIO"), rs.getString("TIPO"),rs.getString("ESTADO"));

            listaServicios.add(servicio);
        }
        return listaServicios;
    }


    public Servicio buscarServicio(Servicio servicio) throws Exception {
        sql ="SELECT CODIGO, DESCRIPCION, PRECIO, TIPO,ESTADO" +
                "FROM SERVICIO " +
                "WHERE CODIGO='"+servicio.getCodigo()+"'";
        ResultSet rs = Conector.getConnector().ejecutarQuery(sql);

        if(rs.next()){
            Servicio servicioEncontrado = new Servicio(rs.getInt("CODIGO"),rs.getString("DESCRIPCION"),
                    rs.getString("PRECIO"), rs.getString("TIPO"),rs.getString("ESTADO"));
            return servicioEncontrado;
        }
        return null;
    }


    public String registrarServicio(Servicio servicio) throws Exception {
        sql = "INSERT INTO SERVICIO VALUES ("+servicio.getCodigo()+",'"+servicio.getDescripcion()+"','"+servicio.getPrecio()+"','"+servicio.getTipo()+"','"+servicio.getEstado()+"')";
        Conector.getConnector().ejecutarSQL(sql);

        return "Se registro el SERVICIO en MySQL";
    }



    public String actualizarServicio(Servicio servicio) throws Exception {
        sql = "UPDATE SERVICIO SET CODIGO= "+servicio.getCodigo()+", DESCRIPCION='"+servicio.getDescripcion()+"',PRECIO='"+servicio.getPrecio()+"', TIPO='"+servicio.getTipo()+"',ESTADO='"+servicio.getEstado()+"' WHERE CODIGO = "+servicio.getCodigo()+"";
        Conector.getConnector().ejecutarSQL(sql);

        return "Se actualizó el SERVICIO en MySQL";
    }

    public String eliminarServicio(Servicio servicio) throws Exception {
        sql = "DELETE FROM SERVICIO WHERE CODIGO="+servicio.getCodigo()+"";
        Conector.getConnector().ejecutarSQL(sql);

        return "Se eliminó el SERVICIO en MySQL";
    }


}
