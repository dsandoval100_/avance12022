package sandoval.daniel.bl.entities.Auto;

import sandoval.daniel.bl.entities.Transporte.Transporte;

import java.util.Objects;


/**
 * @author Daniel Sandoval
 * @version 2.0
 * @since 4-19-2022
 *
 *Esta clase se encarga de gestionar todos los objetos auto .
 *
 * */

public class Auto extends Transporte {

    /**
     * se genara los atributos
     * **/
    private String color;
    private String anio;

    /**
     * se genara el constructor vacio
     * **/
    public Auto() {
    }
    /**
     * se genara el constructor con los parametros
     * **/
    public Auto(String marca, String placa, String tipo, String color, String anio) {
        super(marca, placa, tipo);
        this.color = color;
        this.anio = anio;
    }
    /**
     * se genaran los getters y setters
     * **/
    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getAnio() {
        return anio;
    }

    public void setAnio(String anio) {
        this.anio = anio;
    }
    /**
     * se genara el to string
     * **/
    @Override
    public String toString() {
        return super.toString() + "Auto{" +
                "color='" + color + '\'' +
                ", anio='" + anio + '\'' +
                '}';
    }
    /**
     * se genara el metedo equals
     * **/
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Auto auto = (Auto) o;
        return Objects.equals(color, auto.color) && Objects.equals(anio, auto.anio);
    }

}
