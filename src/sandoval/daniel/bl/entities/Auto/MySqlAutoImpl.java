package sandoval.daniel.bl.entities.Auto;

import sandoval.daniel.bl.entities.Moto.Moto;
import sandoval.daniel.dl.bd.Conector;

import java.sql.ResultSet;
import java.util.ArrayList;

public class MySqlAutoImpl implements IAutoDAO{

    private String sql="";


    public ArrayList<Auto> listarAuto() throws Exception {
        ArrayList<Auto> listaAutos = new ArrayList<>();
        sql="SELECT * FROM AUTO";
        ResultSet rs = Conector.getConnector().ejecutarQuery(sql);

        while (rs.next()){
            Auto auto = new Auto(rs.getString("MARCA"),rs.getString("PLACA"),rs.getString("TIPO"),rs.getString("COLOR"),rs.getString("ANIO"));
            listaAutos.add(auto);
        }
        return listaAutos;
    }

    public Auto buscarAuto(Auto auto) throws Exception {
        sql ="SELECT MARCA, PLACA, TIPO,COLOR,ANIO" +
                "FROM AUTO" +
                "WHERE PLACA='"+auto.getPlaca()+"'";
        ResultSet rs = Conector.getConnector().ejecutarQuery(sql);

        if(rs.next()){
            Auto autoEncontrado = new Auto(rs.getString("MARCA"),rs.getString("PLACA"),rs.getString("TIPO"),rs.getString("COLOR"),rs.getString("ANIO"));
            return autoEncontrado;
        }
        return null;
    }

    public String registrarAuto(Auto auto) throws Exception {
        sql = "INSERT INTO AUTO VALUES ('"+auto.getMarca()+"','"+auto.getPlaca()+"','"+auto.getTipo()+"','"+auto.getColor()+"','"+auto.getAnio()+"')";
        Conector.getConnector().ejecutarSQL(sql);

        return "Se registro el auto en MySQL";
    }


    public String actualizarAuto(Auto auto) throws Exception {
        sql = "UPDATE AUTO SET MARCA='"+auto.getMarca()+"',PLACA='"+auto.getPlaca()+"',TIPO='"+auto.getTipo()+"',COLOR='"+auto.getColor()+"',ANIO='"+auto.getAnio()+"' WHERE PLACA = '"+auto.getPlaca()+"'";
        Conector.getConnector().ejecutarSQL(sql);

        return "Se actualizó el auto en MySQL";
    }

    public String eliminarAuto(Auto auto) throws Exception {
        sql = "DELETE FROM AUTO WHERE PLACA='"+auto.getPlaca()+"'";
        Conector.getConnector().ejecutarSQL(sql);

        return "Se eliminó el auto en MySQL";
    }


}
