package sandoval.daniel.bl.entities.Auto;


import sandoval.daniel.bl.entities.Moto.Moto;

import java.util.ArrayList;

public interface IAutoDAO {
    String registrarAuto(Auto auto) throws Exception;
    String actualizarAuto(Auto auto) throws Exception;
    String eliminarAuto(Auto auto) throws Exception;
    ArrayList<Auto> listarAuto() throws Exception;
    Auto buscarAuto(Auto auto) throws Exception;

}
