package sandoval.daniel.bl.entities.Transporte;


import java.util.ArrayList;

public interface ITransporteDAO {
    String registrarTransporte(Transporte transporte) throws Exception;
    String actualizarTransporte(Transporte transporte) throws Exception;
    String eliminarTransporte(String placa) throws Exception;
    ArrayList<Transporte> listarTransporte() throws Exception;
}
