package sandoval.daniel.bl.entities.Transporte;

import sandoval.daniel.dl.bd.Conector;

import java.util.ArrayList;

public class MySqlTransporteImpl implements ITransporteDAO{

    private String sql;

    public String registrarTransporte(Transporte transporte) throws Exception {
        sql = "INSERT INTO TRANSPORTE VALUES ("+transporte.getMarca()+",'"+transporte.getPlaca()+"')";
        Conector.getConnector().ejecutarSQL(sql);

        return "Se registro el TRANSPORTE en MySQL";
    }



    public String actualizarTransporte(Transporte transporte) throws Exception {
        sql = "UPDATE TRANSPORTE SET MARCA='"+transporte.getMarca()+"' WHERE PLACA = "+transporte.getPlaca()+"";
        Conector.getConnector().ejecutarSQL(sql);

        return "Se actualizó el TRANSPORTE en MySQL";
    }

    public String eliminarTransporte(String placa) throws Exception {
        sql = "DELETE FROM TRANSPORTE WHERE PLACA="+placa+"";
        Conector.getConnector().ejecutarSQL(sql);

        return "Se eliminó el TRANSPORTE en MySQL";
    }

    public ArrayList<Transporte> listarTransporte() throws Exception {
        return new ArrayList<>();
    }

}
