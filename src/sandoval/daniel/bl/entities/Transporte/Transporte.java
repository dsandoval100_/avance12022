package sandoval.daniel.bl.entities.Transporte;

import java.util.Objects;

/**
 * @author Daniel Sandoval
 * @version 2.0
 * @since 4-19-2022
 *
 *Esta clase se encarga de gestionar todos los objetos transporte .
 *
 * */

public class Transporte {
    /**
     * se genara los atributos
     * **/
    private String marca;
    private String placa;
    private String tipo;


    /**
     * se genara el constructor vacio
     * **/
    public Transporte() {
    }
    /**
     * se genara el constructor con los parametros
     * **/
    public Transporte(String marca, String placa, String tipo) {
        this.marca = marca;
        this.placa = placa;
        this.tipo = tipo;
    }
    /**
     * se genaran los getters y setters
     * **/
    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
    /**
     * se genara el to string
     * **/
    @Override
    public String toString() {
        return "Transporte{" +
                "marca='" + marca + '\'' +
                ", placa='" + placa + '\'' +
                ", tipo='" + tipo + '\'' +
                '}';
    }

    /**
     * se genara el metedo equals
     * **/
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Transporte that = (Transporte) o;
        return Objects.equals(marca, that.marca) && Objects.equals(placa, that.placa) && Objects.equals(tipo, that.tipo);
    }


}
