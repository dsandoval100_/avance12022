package sandoval.daniel.bl.entities.Pago;

import sandoval.daniel.bl.entities.Administrador.Administrador;

import java.util.ArrayList;

public interface IPagoDAO {
    String registrarPago(Pago pago) throws Exception;
    String actualizarPago(Pago pago) throws Exception;
    String eliminarPago(Pago pago) throws Exception;
    ArrayList<Pago> listarPago() throws Exception;
}
