package sandoval.daniel.bl.entities.Pago;

import sandoval.daniel.bl.entities.Administrador.Administrador;
import sandoval.daniel.dl.bd.Conector;

import java.sql.ResultSet;
import java.util.ArrayList;

public class MySqlPagoImpl implements IPagoDAO{

    private String sql;

    public String registrarPago(Pago pago) throws Exception {
        sql = "INSERT INTO PAGO VALUES ("+pago.getNumero()+",'"+pago.getProveedor()+"',"+pago.getId()+")";
        Conector.getConnector().ejecutarSQL(sql);

        return "Se registro el PAGO en MySQL";
    }



    public String actualizarPago(Pago pago) throws Exception {
        sql = "UPDATE PAGO SET NUMERO= "+pago.getNumero()+",PROVEEDOR='"+pago.getProveedor()+"',ID="+pago.getId()+" WHERE NUMERO = "+pago.getNumero()+"";
        Conector.getConnector().ejecutarSQL(sql);

        return "Se actualizó el PAGO en MySQL";
    }

    public String eliminarPago(Pago pago) throws Exception {
        sql = "DELETE FROM PAGO WHERE NUMERO="+pago.getNumero()+"";
        Conector.getConnector().ejecutarSQL(sql);

        return "Se eliminó el PAGO en MySQL";
    }

    public ArrayList<Pago> listarPago() throws Exception {
        ArrayList<Pago> listaPago = new ArrayList<>();
        sql="SELECT * FROM PAGO";
        ResultSet rs = Conector.getConnector().ejecutarQuery(sql);

        while (rs.next()){
            Pago pago = new Pago(rs.getInt("NUMERO"), rs.getString("PROVEEDOR"),rs.getInt("id"));
            listaPago.add(pago);
        }
        return listaPago;
    }



}
