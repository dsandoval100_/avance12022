package sandoval.daniel.bl.entities.Pago;

import java.util.Objects;

/**
 * @author Daniel Sandoval
 * @version 2.0
 * @since 4-19-2022
 *
 *Esta clase se encarga de gestionar todos los objetos pago .
 *
 * */

public class Pago {
    /**
     * se genara los atributos
     * **/
    private int numero;
    private String proveedor;
    private int id;

    /**
     * se genara el constructor vacio
     * **/
    public Pago() {
    }
    /**
     * se genara el constructor con los parametros
     * **/
    public Pago(int numero, String proveedor, int id) {
        this.numero = numero;
        this.proveedor = proveedor;
        this.id = id;
    }

    /**
     * se genaran los getters y setters
     * **/
    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getProveedor() {
        return proveedor;
    }

    public void setProveedor(String proveedor) {
        this.proveedor = proveedor;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    /**
     * se genara el to string
     **/
    @Override
    public String toString() {
        return "Pago{" +
                "numero=" + numero +
                ", proveedor='" + proveedor + '\'' +
                ", id=" + id +
                '}';
    }


    /**
     * se genara el metedo equals
     * **/
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pago pago = (Pago) o;
        return numero == pago.numero && Objects.equals(proveedor, pago.proveedor);
    }


}
