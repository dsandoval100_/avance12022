package sandoval.daniel.bl.entities.Conductor;

import sandoval.daniel.bl.entities.Direccion.Direccion;
import sandoval.daniel.bl.entities.Persona.Persona;

import java.time.LocalDate;
import java.util.Objects;

/**
 * @author Daniel Sandoval
 * @version 2.0
 * @since 4-19-2022
 *
 *Esta clase se encarga de gestionar todos los objetos conductor .
 *
 * */
public class Conductor extends Persona {

    /**
     * se genara los atributos
     * **/
    private String avatar;
    private LocalDate fecha;
    private int edad;
    private String estado;

    /**
     * se genara el constructor vacio
     * **/
    public Conductor() {
    }
    /**
     * se genara el constructor con los parametros
     * **/
    public Conductor(String nombre, String apellido, int id, String usuario, String clave, String direccionExacta, String provincia, String canton, String distrito,String tipoPersona, String avatar, LocalDate fecha, int edad, String estado) {
        super(nombre, apellido, id, usuario, clave, direccionExacta, provincia, canton, distrito,tipoPersona);
        this.avatar = avatar;
        this.fecha = fecha;
        this.edad = edad;
        this.estado = estado;
        this.setTipoPersona("Conductor");
    }

    /**
     * se genaran los getters y setters
     * **/
    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public LocalDate getFecha() {
        return fecha;
    }

    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
    /**
     * se genara el to string
     * **/
    public String toString() {
        return super.toString() + "Conductor{" +
                "avatar='" + avatar + '\'' +
                ", fecha=" + fecha +
                ", edad=" + edad +
                ", estado='" + estado + '\'' +
                '}';
    }
    /**
     * se genara el metedo equals
     * **/
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Conductor conductor = (Conductor) o;
        return edad == conductor.edad && Objects.equals(avatar, conductor.avatar) && Objects.equals(fecha, conductor.fecha) && Objects.equals(estado, conductor.estado);
    }

}
