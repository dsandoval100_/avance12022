package sandoval.daniel.bl.entities.Conductor;

import sandoval.daniel.bl.entities.Administrador.Administrador;
import sandoval.daniel.dl.bd.Conector;

import java.sql.ResultSet;
import java.util.ArrayList;

public class MySqlConductorImpl implements IConductorDAO{
    private String sql="";

    public ArrayList<Conductor> listarConductor() throws Exception {

        ArrayList<Conductor> listaConductores = new ArrayList<>();
        sql="SELECT NOMBRE,APELLIDO,ID,USUARIO,CLAVE,DIRECCIONEXACTA,PROVINCIA,CANTON,DISTRITO,TIPOPERSONA,AVATAR,FECHA,EDAD,ESTADO FROM CONDUCTOR ORDER BY ID ASC";
        ResultSet rs = Conector.getConnector().ejecutarQuery(sql);

        while (rs.next()){
            Conductor conductor = new Conductor(rs.getString("NOMBRE"),rs.getString("APELLIDO"),rs.getInt("ID"),rs.getString("USUARIO"),
                    rs.getString("CLAVE"), rs.getString("DIRECCIONEXACTA"), rs.getString("PROVINCIA"), rs.getString("CANTON"),rs.getString("DISTRITO"),rs.getString("TIPOPERSONA"),
                    rs.getString("AVATAR"),rs.getDate("FECHA").toLocalDate(),
                    rs.getInt("EDAD"),rs.getString("ESTADO"));
            listaConductores.add(conductor);
        }
        return listaConductores;
    }

    public Conductor buscarConductor(Conductor conductor) throws Exception {
        sql ="SELECT NOMBRE, APELLIDO, ID, USUARIO, CLAVE, DIRECCIONEXACTA, PROVINCIA, CANTON,DISTRITO,TIPOPERSONA, AVATAR,FECHA, EDAD, ESTADO " +
                "FROM CONDUCTOR " +
                "WHERE USUARIO='"+conductor.getUsuario()+"' AND CLAVE='"+conductor.getClave()+"'";
        ResultSet rs = Conector.getConnector().ejecutarQuery(sql);

        if(rs.next()){
            Conductor conductorEncontrado = new Conductor(rs.getString("NOMBRE"),rs.getString("APELLIDO"),
                    rs.getInt("ID"), rs.getString("USUARIO"),rs.getString("CLAVE"),
                    rs.getString("DIRECCIONEXACTA"),rs.getString("PROVINCIA"),rs.getString("CANTON"),rs.getString("DISTRITO"),rs.getString("TIPOPERSONA"), rs.getString("AVATAR"),rs.getDate("FECHA").toLocalDate(),
                    rs.getInt("EDAD"),rs.getString("ESTADO"));
            return conductorEncontrado;
        }
        return null;
    }

    public String registrarConductor(Conductor conductor) throws Exception {
        sql = "INSERT INTO CONDUCTOR (NOMBRE,APELLIDO,ID,USUARIO,CLAVE,DIRECCIONEXACTA,PROVINCIA,CANTON,DISTRITO,TIPOPERSONA,AVATAR,FECHA,EDAD,ESTADO)" +
                "VALUES ('"+conductor.getNombre()+"','"+conductor.getApellido()+"',"+conductor.getId()+"," +
                "'"+conductor.getUsuario()+"','"+conductor.getClave()+"'," +
                "'"+conductor.getDireccionExacta()+"','"+conductor.getProvincia()+"','"+conductor.getCanton()+"'," +
                "'"+conductor.getDistrito()+"','"+conductor.getTipoPersona()+"','"+conductor.getAvatar()+"'," +
                "'"+conductor.getFecha()+"',"+conductor.getEdad()+",'"+conductor.getEstado()+"')";
        Conector.getConnector().ejecutarSQL(sql);

        return "Se registro el CONDUCTOR en MySQL";
    }



    public String actualizarConductor(Conductor conductor) throws Exception {
        sql = "UPDATE CONDUCTOR SET NOMBRE='"+conductor.getNombre()+"'," +
                "APELLIDO='"+conductor.getApellido()+"',ID="+conductor.getId()+"," +
                "USUARIO='"+conductor.getUsuario()+"'," +
                "CLAVE='"+conductor.getClave()+"',DIRECCIONEXACTA='"+conductor.getDireccionExacta()+"'," +
                "PROVINCIA='"+conductor.getProvincia()+"',CANTON='"+conductor.getCanton()+"'," +
                "DISTRITO='"+conductor.getDistrito()+"',TIPOPERSONA='"+conductor.getTipoPersona()+"'," +
                "AVATAR='"+conductor.getAvatar()+"',FECHA='"+conductor.getFecha()+"',EDAD="+conductor.getEdad()+"," +
                "ESTADO='"+conductor.getEstado()+"' WHERE ID = "+conductor.getId()+"";
        Conector.getConnector().ejecutarSQL(sql);

        return "Se actualizó el CONDUCTOR en MySQL";
    }

    public String eliminarConductor(Conductor conductor) throws Exception {
        sql = "DELETE FROM CONDUCTOR WHERE ID="+conductor.getId()+"";
        Conector.getConnector().ejecutarSQL(sql);

        return "Se eliminó la persona en MySQL";
    }

}
