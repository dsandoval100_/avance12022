package sandoval.daniel.bl.entities.Conductor;



import java.util.ArrayList;

public interface IConductorDAO {
    String registrarConductor(Conductor conductor) throws Exception;
    Conductor buscarConductor(Conductor conductor) throws Exception;
    String actualizarConductor(Conductor conductor) throws Exception;
    String eliminarConductor(Conductor conductor) throws Exception;
    ArrayList<Conductor> listarConductor() throws Exception;
}
