package sandoval.daniel.bl.entities.Usuario;



import sandoval.daniel.bl.entities.Pago.Pago;

import java.util.ArrayList;

public interface IUsuarioDAO {
    String registrarUsuario(Usuario usuario) throws Exception;
    String actualizarUsuario(Usuario usuario) throws Exception;
    String eliminarUsuario(Usuario usuario) throws Exception;
    Usuario buscarUsuario(Usuario usuario) throws Exception;
    ArrayList<Usuario> listarUsuario() throws Exception;

 String asociarPagoAUsuario(Pago pago, Usuario usuario);
}
