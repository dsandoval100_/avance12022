package sandoval.daniel.bl.entities.Usuario;

import sandoval.daniel.bl.entities.Administrador.Administrador;
import sandoval.daniel.bl.entities.Conductor.Conductor;
import sandoval.daniel.bl.entities.Pago.Pago;
import sandoval.daniel.dl.bd.Conector;

import java.sql.ResultSet;
import java.util.ArrayList;

public class MySqlUsuarioImpl implements IUsuarioDAO{

    private String sql="";

    public ArrayList<Usuario> listarUsuario() throws Exception {

        ArrayList<Usuario> listaUsuarios = new ArrayList<>();
        sql="SELECT NOMBRE,APELLIDO,ID,USUARIO,CLAVE, DIRECCIONEXACTA,PROVINCIA,CANTON,DISTRITO,TIPOPERSONA,AVATAR,FECHA,EDAD,GENERO,CELULAR FROM USUARIO ORDER BY ID ASC";
        ResultSet rs = Conector.getConnector().ejecutarQuery(sql);

        while (rs.next()){
            Usuario usuario = new Usuario(rs.getString("NOMBRE"),rs.getString("APELLIDO"),rs.getInt("ID"),rs.getString("USUARIO"),
                    rs.getString("CLAVE"), rs.getString("DIRECCIONEXACTA"),rs.getString("PROVINCIA"),rs.getString("CANTON"),
                    rs.getString("DISTRITO"),rs.getString("TIPOPERSONA"),
                    rs.getString("AVATAR"),rs.getDate("FECHA").toLocalDate(),
                    rs.getInt("EDAD"),rs.getString("GENERO"),rs.getInt("CELULAR"));
                listaUsuarios.add(usuario);
        }
        return listaUsuarios;
    }

    @Override
    public String asociarPagoAUsuario(Pago pago, Usuario usuario) {
        return null;
    }

    public Usuario buscarUsuario(Usuario usuario) throws Exception {
        sql ="SELECT NOMBRE, APELLIDO, ID, USUARIO, CLAVE ,DIRECCIONEXACTA, PROVINCIA, CANTON, DISTRITO,TIPOPERSONA,AVATAR,FECHA,EDAD,GENERO,CELULAR" +
                "FROM USUARIO " +
                "WHERE USUARIO='"+usuario.getUsuario()+"' AND CLAVE='"+usuario.getClave()+"'";
        ResultSet rs = Conector.getConnector().ejecutarQuery(sql);

        if(rs.next()){
            Usuario usuarioEncontrado = new Usuario(rs.getString("NOMBRE"),rs.getString("APELLIDO"),rs.getInt("ID"),
                    rs.getString("USUARIO"),rs.getString("CLAVE"),rs.getString("DIRECCIONEXACTA"),
                    rs.getString("PROVINCIA"),rs.getString("CANTON"),rs.getString("DISTRITO"),rs.getString("TIPOPERSONA"),
                    rs.getString("AVATAR"),rs.getDate("FECHA").toLocalDate(),
                    rs.getInt("EDAD"),rs.getString("GENERO"),rs.getInt("CELULAR"));
            return usuarioEncontrado;
        }
        return null;
    }

    public String registrarUsuario(Usuario usuario) throws Exception {
        sql = "INSERT INTO USUARIO (NOMBRE,APELLIDO,ID,USUARIO,CLAVE,DIRECCIONEXACTA,PROVINCIA,CANTON,DISTRITO,TIPOPERSONA,AVATAR,FECHA,EDAD,GENERO,CELULAR)" +
                "VALUES ('"+usuario.getNombre()+"','"+usuario.getApellido()+"',"+usuario.getId()+"," +
                "'"+usuario.getUsuario()+"','"+usuario.getClave()+"'," +
                "'"+usuario.getDireccionExacta()+"','"+usuario.getProvincia()+"','"+usuario.getCanton()+"','"+usuario.getDistrito()+"','"+usuario.getTipoPersona()+"','"+usuario.getAvatar()+"'," +
                "'"+usuario.getFecha()+"',"+usuario.getEdad()+",'"+usuario.getGenero()+"',"+usuario.getCelular()+")";
        Conector.getConnector().ejecutarSQL(sql);

        return "Se registro el USUARIO en MySQL";
    }

    public String actualizarUsuario(Usuario usuario) throws Exception {
        sql = "UPDATE USUARIO SET NOMBRE='"+usuario.getNombre()+"',APELLIDO='"+usuario.getApellido()+"',ID="+usuario.getId()+",USUARIO='"+usuario.getUsuario()+"',CLAVE='"+usuario.getClave()+"',DIRECCIONEXACTA='"+usuario.getDireccionExacta()+"',PROVINCIA='"+usuario.getProvincia()+"',CANTON='"+usuario.getCanton()+"',DISTRITO='"+usuario.getDistrito()+"',TIPOPERSONA='"+usuario.getTipoPersona()+"',AVATAR='"+usuario.getAvatar()+"',FECHA='"+usuario.getFecha()+"',EDAD="+usuario.getEdad()+",GENERO='"+usuario.getGenero()+"',CELULAR="+usuario.getCelular()+" WHERE ID = "+usuario.getId()+"";
        Conector.getConnector().ejecutarSQL(sql);

        return "Se actualizó el USUARIO en MySQL";
    }




    public String eliminarUsuario(Usuario usuario) throws Exception {
        sql = "DELETE FROM USUARIO WHERE ID="+usuario.getId()+"";
        Conector.getConnector().ejecutarSQL(sql);

        return "Se eliminó la persona en MySQL";
    }

}
