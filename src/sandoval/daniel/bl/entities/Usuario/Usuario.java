package sandoval.daniel.bl.entities.Usuario;

import sandoval.daniel.bl.entities.Pago.Pago;
import sandoval.daniel.bl.entities.Persona.Persona;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Objects;

/**
 * @author Daniel Sandoval
 * @version 2.0
 * @since 4-19-2022
 *
 *Esta clase se encarga de gestionar todos los objetos usuario o cliente .
 *
 * */

public class Usuario extends Persona {

    /**
     * se genara los atributos
     * **/
    private String avatar;
    private LocalDate fecha;
    private int edad;
    private String genero;
    private int celular;




    /**
     *  se genara el constructor vacio
     *  **/


     public Usuario() {
     }

    /**
     *  se genara el constructor con parametros
     *  **/

    public Usuario(String nombre, String apellido, int id, String usuario, String clave, String direccionExacta, String provincia, String canton, String distrito, String tipoPersona, String avatar, LocalDate fecha, int edad, String genero, int celular) {
        super(nombre, apellido, id, usuario, clave, direccionExacta, provincia, canton, distrito, tipoPersona);
        this.avatar = avatar;
        this.fecha = fecha;
        this.edad = edad;
        this.genero = genero;
        this.celular = celular;
        this.setTipoPersona("Usuario");
    }

    /**
     * se genaran los getters y setters
     * **/
    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public LocalDate getFecha() {
        return fecha;
    }

    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public int getCelular() {
        return celular;
    }

    public void setCelular(int celular) {
        this.celular = celular;
    }


    /**
     * se genara el to string
     * **/
    @Override
    public String toString() {
        return super.toString() + "Usuario{" +
                "avatar='" + avatar + '\'' +
                ", fecha=" + fecha +
                ", edad=" + edad +
                ", genero='" + genero + '\'' +
                ", celular='" + celular + '\'' +
                '}';
    }

    /**
     * se genara el metedo equals
     * **/
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Usuario usuario = (Usuario) o;
        return edad == usuario.edad && celular == usuario.celular && Objects.equals(avatar, usuario.avatar) && Objects.equals(fecha, usuario.fecha) && Objects.equals(genero, usuario.genero);
    }

}
