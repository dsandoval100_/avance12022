package sandoval.daniel.bl.entities.Provincia;


import java.util.ArrayList;

public interface IProvinciaDAO {
    String registrarProvincia(Provincia provincia) throws Exception;
    String actualizarProvincia(Provincia provincia) throws Exception;
    String eliminarProvincia(int id) throws Exception;
    ArrayList<Provincia> listarProvincia() throws Exception;
}
