package sandoval.daniel.bl.entities.Provincia;

import sandoval.daniel.bl.entities.Direccion.Direccion;

/**
 * @author Daniel Sandoval
 * @version 2.0
 * @since 4-19-2022
 *
 *Esta clase se encarga de gestionar todos los objetos provincia .
 *
 * */

public class Provincia extends Direccion {

    /**
     * se genara el constructor vacio
     * **/
    public Provincia() {
    }
    /**
     * se genara el constructor con los parametros
     * **/
    public Provincia(String nombre, int id, String dirreccionExacta) {
        super(nombre, id, dirreccionExacta);
    }
    /**
     * se genara el to string
     * **/
    @Override
    public String toString() {
        return super.toString() + "Provincia{}";
    }
    /**
     * se genara el metedo equals
     * **/
    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }
}
