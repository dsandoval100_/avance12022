package sandoval.daniel.bl.entities.Administrador;

import sandoval.daniel.bl.entities.Persona.Persona;
import sandoval.daniel.dl.bd.Conector;

import java.sql.ResultSet;
import java.util.ArrayList;

public class MySqlAdministradorImpl implements IAdministradorDAO{

    private String sql="";

    public ArrayList<Administrador> listarAdministradores() throws Exception {

        ArrayList<Administrador> listaAdministradores = new ArrayList<>();
        sql="SELECT * FROM ADMINISTRADOR ORDER BY ID ASC";
        ResultSet rs = Conector.getConnector().ejecutarQuery(sql);

        while (rs.next()){
            Administrador administrador = new Administrador(rs.getString("NOMBRE"),rs.getString("APELLIDO"),rs.getInt("ID"),rs.getString("USUARIO"),rs.getString("CLAVE"), rs.getString("DIRECCIONEXACTA"),rs.getString("PROVINCIA"),rs.getString("CANTON"),rs.getString("DISTRITO"),rs.getString("TIPOPERSONA"));
            listaAdministradores.add(administrador);
        }
        return listaAdministradores;
    }

    public Administrador buscarAdministrador(Administrador administrador) throws Exception {
        sql ="SELECT NOMBRE, APELLIDO, ID, USUARIO, CLAVE ,DIRECCIONEXACTA, PROVINCIA, CANTON, DISTRITO,TIPOPERSONA" +
                "FROM ADMINISTRADOR " +
                "WHERE USUARIO='"+administrador.getUsuario()+"' AND CLAVE='"+administrador.getClave()+"'";
        ResultSet rs = Conector.getConnector().ejecutarQuery(sql);

        if(rs.next()){
            Administrador administradorEncontrado = new Administrador(rs.getString("NOMBRE"),rs.getString("APELLIDO"),rs.getInt("ID"),
                    rs.getString("USUARIO"),rs.getString("CLAVE"),rs.getString("DIRECCIONEXACTA"),rs.getString("PROVINCIA"),rs.getString("CANTON"),rs.getString("DISTRITO"),rs.getString("TIPOPERSONA"));
            return administradorEncontrado;
        }
        return null;
    }

    public String registrarAdministrador(Administrador administrador) throws Exception {
        sql = "INSERT INTO ADMINISTRADOR(NOMBRE, APELLIDO, ID, USUARIO, CLAVE ,DIRECCIONEXACTA, PROVINCIA, CANTON, DISTRITO,TIPOPERSONA)" + " VALUES ('"+administrador.getNombre()+"','"+administrador.getApellido()+"',"+administrador.getId()+",'"+administrador.getUsuario()+"','"+administrador.getClave()+"','"+administrador.getDireccionExacta()+"','"+administrador.getProvincia()+"','"+administrador.getCanton()+"','"+administrador.getDistrito()+"','"+administrador.getTipoPersona()+"')";
        Conector.getConnector().ejecutarSQL(sql);

        return "Se registro la persona en MySQL";
    }



    public String actualizarAdministrador(Administrador administrador) throws Exception {
        sql = "UPDATE ADMINISTRADOR SET NOMBRE='"+administrador.getNombre()+"',APELLIDO='"+administrador.getApellido()+"',ID='"+administrador.getId()+"',USUARIO='"+administrador.getUsuario()+"',CLAVE='"+administrador.getClave()+"',DIRECCIONEXACTA='"+administrador.getDireccionExacta()+"',PROVINCIA='"+administrador.getProvincia()+"',CANTON='"+administrador.getCanton()+"',DISTRITO='"+administrador.getDistrito()+"',TIPOPERSONA='"+administrador.getTipoPersona()+"' WHERE ID = "+administrador.getId()+"";
        Conector.getConnector().ejecutarSQL(sql);

        return "Se actualizó la persona en MySQL";
    }



    public String eliminarAdministrador(Administrador administrador) throws Exception {
        sql = "DELETE FROM ADMINISTRADOR WHERE ID="+administrador.getId()+"";
        Conector.getConnector().ejecutarSQL(sql);

        return "Se eliminó la persona en MySQL";
    }


}
