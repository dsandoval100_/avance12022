package sandoval.daniel.bl.entities.Administrador;

import java.util.ArrayList;

public interface IAdministradorDAO {
    ArrayList<Administrador> listarAdministradores() throws Exception;
    Administrador buscarAdministrador(Administrador administrador) throws Exception;
    String registrarAdministrador(Administrador administrador) throws Exception;
    String actualizarAdministrador(Administrador administrador) throws Exception;
    String eliminarAdministrador(Administrador administrador) throws Exception;

}
