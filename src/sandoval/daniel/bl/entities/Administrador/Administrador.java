package sandoval.daniel.bl.entities.Administrador;

import sandoval.daniel.bl.entities.Direccion.Direccion;
import sandoval.daniel.bl.entities.Persona.Persona;

/**
 * @author Daniel Sandoval
 * @version 2.0
 * @since 4-19-2022
 *
 *Esta clase se encarga de gestionar todos los objetos administradores .
 *
 * */
public class Administrador extends Persona {

    /**
     * se genara el constructor vacio
     * **/
    public Administrador() {
    }
    /**
     * se genara el constructor con los parametros
     * **/
    public Administrador(String nombre, String apellido, int id, String usuario, String clave, String direccionExacta, String provincia, String canton, String distrito, String tipoPersona) {
        super(nombre, apellido, id, usuario, clave, direccionExacta, provincia, canton, distrito, tipoPersona);
        this.setTipoPersona("Administrador");
    }



    /**
     * se genara el to string
     * **/
    @Override
    public String toString() {
        return super.toString() + "Administrador{}";
    }

    /**
     * se genara el metedo equals
     * **/

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }
}
