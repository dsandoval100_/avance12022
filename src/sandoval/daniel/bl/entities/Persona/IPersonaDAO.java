package sandoval.daniel.bl.entities.Persona;


import java.util.ArrayList;

public interface IPersonaDAO {

    Persona verificarAcceso(Persona usuarioBuscado)throws Exception;

    ArrayList<Persona> listarAdministradrores()throws Exception;
    ArrayList<Persona> listarConductores()throws Exception;
    ArrayList<Persona> listarUsuarios()throws Exception;


}