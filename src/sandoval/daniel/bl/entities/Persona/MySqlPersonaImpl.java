package sandoval.daniel.bl.entities.Persona;

import sandoval.daniel.bl.entities.Usuario.Usuario;
import sandoval.daniel.dl.bd.Conector;

import java.sql.ResultSet;
import java.util.ArrayList;

public class MySqlPersonaImpl implements IPersonaDAO{

    private String sql="";


    @Override
    public Persona verificarAcceso(Persona usuarioBuscado) throws Exception {
        ArrayList<Persona> listaAdministradores;
        ArrayList<Persona> listaConductores;
        ArrayList<Persona> listaUsuarios;
        listaAdministradores = listarAdministradrores();
        listaConductores = listarConductores();
        listaUsuarios = listarUsuarios();


        for (Persona personaAd : listarAdministradrores()){
            if(personaAd.equalsID(usuarioBuscado)){
                return personaAd;
            }
        }
        for (Persona personaAd : listarConductores()){
            if (personaAd.equalsID(usuarioBuscado)){
                return personaAd;
            }
        }
        for (Persona personaAd : listarUsuarios()){
            if (personaAd.equalsID(usuarioBuscado)){
                return personaAd;
            }
        }
        usuarioBuscado.setTipoPersona("Error");
        return usuarioBuscado;
    }

    @Override
    public ArrayList<Persona> listarAdministradrores() throws Exception {
        ArrayList<Persona> lista = new ArrayList<>();

        sql="SELECT nombre, apellido, id, usuario, clave,direccionexacta,provincia,canton,distrito,tipopersona FROM Administrador";
        ResultSet rs = Conector.getConnector().ejecutarQuery(sql);

        while (rs.next()){
            Persona persona = new Persona();
            persona.setNombre(rs.getString("nombre"));
            persona.setApellido(rs.getString("apellido"));
            persona.setId(rs.getInt("id"));
            persona.setUsuario(rs.getString("usuario"));
            persona.setClave(rs.getString("clave"));
            persona.setDireccionExacta(rs.getString("direccionexacta"));
            persona.setProvincia(rs.getString("provincia"));
            persona.setCanton(rs.getString("canton"));
            persona.setDistrito(rs.getString("distrito"));
            persona.setTipoPersona(rs.getString("tipopersona"));
            lista.add(persona);

        }

        return lista;
    }

    @Override
    public ArrayList<Persona> listarConductores() throws Exception {
        ArrayList<Persona> lista = new ArrayList<>();

        sql="Select * from Conductor";
        ResultSet rs = Conector.getConnector().ejecutarQuery(sql);

        while (rs.next()){
            Persona persona = new Persona();
            persona.setNombre(rs.getString("nombre"));
            persona.setApellido(rs.getString("apellido"));
            persona.setId(rs.getInt("id"));
            persona.setUsuario(rs.getString("usuario"));
            persona.setClave(rs.getString("clave"));
            persona.setDireccionExacta(rs.getString("direccionexacta"));
            persona.setProvincia(rs.getString("provincia"));
            persona.setCanton(rs.getString("canton"));
            persona.setDistrito(rs.getString("distrito"));
            persona.setTipoPersona(rs.getString("tipopersona"));
            lista.add(persona);

        }


        return lista;
    }

    @Override
    public ArrayList<Persona> listarUsuarios() throws Exception {
        ArrayList<Persona> lista = new ArrayList<>();

        sql="Select * from Usuario";
        ResultSet rs = Conector.getConnector().ejecutarQuery(sql);

        while (rs.next()){
            Persona persona = new Persona();
            persona.setNombre(rs.getString("nombre"));
            persona.setApellido(rs.getString("apellido"));
            persona.setId(rs.getInt("id"));
            persona.setUsuario(rs.getString("usuario"));
            persona.setClave(rs.getString("clave"));
            persona.setDireccionExacta(rs.getString("direccionexacta"));
            persona.setProvincia(rs.getString("provincia"));
            persona.setCanton(rs.getString("canton"));
            persona.setDistrito(rs.getString("distrito"));
            persona.setTipoPersona(rs.getString("tipopersona"));
            lista.add(persona);

        }

        return lista;
    }


}
