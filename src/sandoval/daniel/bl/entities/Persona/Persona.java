package sandoval.daniel.bl.entities.Persona;


import sandoval.daniel.bl.entities.Direccion.Direccion;

import java.util.Objects;

/**
 * @author Daniel Sandoval
 * @version 2.0
 * @since 4-19-2022
 *
 *Esta clase se encarga de gestionar todos los objetos persona y es clase padres .
 *
 * */

public class Persona {

    /**
     * se genara los atributos
     * **/
    private String nombre;
    private String apellido;
    private int id;
    private String usuario;
    private String clave;
    private Direccion direccion;
    private String direccionExacta;
    private String provincia;
    private String canton;
    private String distrito;
    private String tipoPersona;


    /**
     * se genara el constructor vacio
     * **/
    public Persona() {
    }
    /**
     * se genara el constructor con los parametros
     * **/
    public Persona(String nombre, String apellido, int id, String usuario, String clave, String direccionExacta, String provincia, String canton, String distrito,String tipoPersona) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.id = id;
        this.usuario = usuario;
        this.clave = clave;
        this.direccion = direccion;
        this.direccionExacta = direccionExacta;
        this.provincia = provincia;
        this.canton = canton;
        this.distrito = distrito;
        this.tipoPersona =tipoPersona;
    }

    /**
     * se genaran los getters y setters
     * **/
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public Direccion getDireccion() {
        return direccion;
    }

    public void setDireccion(Direccion direccion) {
        this.direccion = direccion;
    }

    public String getDireccionExacta() {
        return direccionExacta;
    }

    public void setDireccionExacta(String direccionExacta) {
        this.direccionExacta = direccionExacta;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getCanton() {
        return canton;
    }

    public void setCanton(String canton) {
        this.canton = canton;
    }

    public String getDistrito() {
        return distrito;
    }

    public void setDistrito(String distrito) {
        this.distrito = distrito;
    }

    public String getTipoPersona() {
        return tipoPersona;
    }

    public void setTipoPersona(String tipoPersona) {
        this.tipoPersona = tipoPersona;
    }

    /**
     * se genara el to string
     * **/
    @Override
    public String toString() {
        return "Persona{" +
                "nombre='" + nombre + '\'' +
                ", apellido='" + apellido + '\'' +
                ", id=" + id +
                ", usuario='" + usuario + '\'' +
                ", clave='" + clave + '\'' +
                ", direccion=" + direccion +
                ", direccionExacta='" + direccionExacta + '\'' +
                ", provincia='" + provincia + '\'' +
                ", canton='" + canton + '\'' +
                ", distrito='" + distrito + '\'' +
                ", tipoPersona='" + tipoPersona + '\'' +

                '}';
    }

    /**
     * se genara el metedo equals
     * **/
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Persona persona = (Persona) o;
        return id == persona.id && Objects.equals(nombre, persona.nombre) && Objects.equals(apellido, persona.apellido) && Objects.equals(usuario, persona.usuario) && Objects.equals(clave, persona.clave) && Objects.equals(direccion, persona.direccion) && Objects.equals(direccionExacta, persona.direccionExacta);
    }

    public boolean equalsID(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Persona persona = (Persona) o;
        return Objects.equals(getUsuario(), persona.getUsuario()) && Objects.equals(getClave(), persona.getClave());
    }


}
