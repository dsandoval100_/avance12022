package sandoval.daniel.bl.entities.Direccion;


import java.util.ArrayList;

public interface IDireccionDAO {
    String registrarDireccion(Direccion direccion) throws Exception;
    String actualizarDireccion(Direccion direccion) throws Exception;
    String eliminarDireccion(int id) throws Exception;
    ArrayList<Direccion> listarDireccion() throws Exception;
}
