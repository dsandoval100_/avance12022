package sandoval.daniel.bl.entities.Direccion;

import java.util.Objects;

/**
 * @author Daniel Sandoval
 * @version 2.0
 * @since 4-19-2022
 *
 *Esta clase se encarga de gestionar todos los objetos direccion .
 *
 * */

public class Direccion {

    /**
     * se genara los atributos
     * **/
    private String nombre;
    private int id;
    private String dirreccionExacta;

    /**
     * se genara el constructor vacio
     * **/
    public Direccion() {
    }
    /**
     * se genara el constructor con los parametros
     * **/
    public Direccion(String nombre, int id, String dirreccionExacta) {
        this.nombre = nombre;
        this.id = id;
        this.dirreccionExacta = dirreccionExacta;
    }

    /**
     * se genaran los getters y setters
     * **/
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDirreccionExacta() {
        return dirreccionExacta;
    }

    public void setDirreccionExacta(String dirreccionExacta) {
        this.dirreccionExacta = dirreccionExacta;
    }
    /**
     * se genara el to string
     * **/
    @Override
    public String toString() {
        return "Direccion{" +
                "nombre='" + nombre + '\'' +
                ", id=" + id +
                ", dirreccionExacta='" + dirreccionExacta + '\'' +
                '}';
    }
    /**
     * se genara el metedo equals
     * **/
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Direccion direccion = (Direccion) o;
        return id == direccion.id && Objects.equals(nombre, direccion.nombre) && Objects.equals(dirreccionExacta, direccion.dirreccionExacta);
    }


}
