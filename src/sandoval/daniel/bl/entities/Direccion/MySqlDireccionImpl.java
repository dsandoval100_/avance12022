package sandoval.daniel.bl.entities.Direccion;

import sandoval.daniel.dl.bd.Conector;

import java.util.ArrayList;

public class MySqlDireccionImpl implements IDireccionDAO{

    private String sql;

    public String registrarDireccion(Direccion direccion) throws Exception {
        sql = "INSERT INTO DIRECCION VALUES ("+direccion.getNombre()+",'"+direccion.getId()+"')";
        Conector.getConnector().ejecutarSQL(sql);

        return "Se registro el DIRECCION en MySQL";
    }



    public String actualizarDireccion(Direccion direccion) throws Exception {
        sql = "UPDATE DIRECCION SET NOMBRE='"+direccion.getNombre()+"' WHERE ID = "+direccion.getId()+"";
        Conector.getConnector().ejecutarSQL(sql);

        return "Se actualizó el DIRECCION en MySQL";
    }

    public String eliminarDireccion(int id) throws Exception {
        sql = "DELETE FROM DIRECCION WHERE ID="+id+"";
        Conector.getConnector().ejecutarSQL(sql);

        return "Se eliminó el DIRECCION en MySQL";
    }

    public ArrayList<Direccion> listarDireccion() throws Exception {
        return new ArrayList<>();
    }

}
