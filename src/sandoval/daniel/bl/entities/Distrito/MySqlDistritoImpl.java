package sandoval.daniel.bl.entities.Distrito;

import sandoval.daniel.dl.bd.Conector;

import java.util.ArrayList;

public class MySqlDistritoImpl implements IDistritoDAO {

    private String sql;

    public String registrarDistrito(Distrito distrito) throws Exception {
        sql = "INSERT INTO DISTRITO VALUES ("+distrito.getNombre()+",'"+distrito.getId()+"')";
        Conector.getConnector().ejecutarSQL(sql);

        return "Se registro el DISTRITO en MySQL";
    }



    public String actualizarDistrito(Distrito distrito) throws Exception {
        sql = "UPDATE DISTRITO SET NOMBRE='"+distrito.getNombre()+"' WHERE ID = "+distrito.getId()+"";
        Conector.getConnector().ejecutarSQL(sql);

        return "Se actualizó el DISTRITO en MySQL";
    }

    public String eliminarDistrito(int id) throws Exception {
        sql = "DELETE FROM DISTRITO WHERE ID="+id+"";
        Conector.getConnector().ejecutarSQL(sql);

        return "Se eliminó el DISTRITO en MySQL";
    }

    public ArrayList<Distrito> listarDistrito() throws Exception {
        return new ArrayList<>();
    }
}
