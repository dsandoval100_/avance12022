package sandoval.daniel.bl.entities.Distrito;


import java.util.ArrayList;

public interface IDistritoDAO {
    String registrarDistrito(Distrito distrito) throws Exception;
    String actualizarDistrito(Distrito distrito) throws Exception;
    String eliminarDistrito(int id) throws Exception;
    ArrayList<Distrito> listarDistrito() throws Exception;
}
