package sandoval.daniel.bl.entities.Canton;

import sandoval.daniel.dl.bd.Conector;

import java.util.ArrayList;

public class MySqlCantonImpl implements ICantonDAO{

    private String sql;

    public String registrarCanton(Canton canton) throws Exception {
        sql = "INSERT INTO CANTON VALUES ("+canton.getNombre()+",'"+canton.getId()+"')";
        Conector.getConnector().ejecutarSQL(sql);

        return "Se registro el CANTON en MySQL";
    }



    public String actualizarCanton(Canton canton) throws Exception {
        sql = "UPDATE CANTON SET NOMBRE='"+canton.getNombre()+"' WHERE ID = "+canton.getId()+"";
        Conector.getConnector().ejecutarSQL(sql);

        return "Se actualizó el CANTON en MySQL";
    }

    public String eliminarCanton(int id) throws Exception {
        sql = "DELETE FROM CANTON WHERE ID="+id+"";
        Conector.getConnector().ejecutarSQL(sql);

        return "Se eliminó el CANTON en MySQL";
    }

    public ArrayList<Canton> listarCanton() throws Exception {
        return new ArrayList<>();
    }
}
