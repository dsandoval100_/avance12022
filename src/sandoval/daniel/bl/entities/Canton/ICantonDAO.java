package sandoval.daniel.bl.entities.Canton;

import java.util.ArrayList;

public interface ICantonDAO {
    String registrarCanton(Canton canton) throws Exception;
    String actualizarCanton(Canton canton) throws Exception;
    String eliminarCanton(int id) throws Exception;
    ArrayList<Canton> listarCanton() throws Exception;
}

