package sandoval.daniel.bl.entities.Canton;

import sandoval.daniel.bl.entities.Direccion.Direccion;

/**
 * @author Daniel Sandoval
 * @version 2.0
 * @since 4-19-2022
 *
 *Esta clase se encarga de gestionar todos los objetos canton .
 *
 * */

public class Canton extends Direccion {

    /**
     * se genara el constructor vacio
     * **/
    public Canton() {
    }

    /**
     * se genara el constructor con los parametros
     * **/
    public Canton(String nombre, int id, String dirreccionExacta) {
        super(nombre, id, dirreccionExacta);
    }
    /**
     * se genara el to string
     * **/
    @Override
    public String toString() {
        return super.toString() + "Canton{}";
    }
    /**
     * se genara el metedo equals
     * **/
    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }
}
