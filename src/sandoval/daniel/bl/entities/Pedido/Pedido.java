package sandoval.daniel.bl.entities.Pedido;

import sandoval.daniel.bl.entities.Direccion.Direccion;

import java.time.LocalDate;
import java.util.Objects;

/**
 * @author Daniel Sandoval
 * @version 2.0
 * @since 4-19-2022
 *
 *Esta clase se encarga de gestionar todos los objetos pedidos .
 *
 * */

public class Pedido {
    /**
     * se genara los atributos
     * **/
    private int numero;
    private LocalDate fechaCreacion;
    private String Datos;
    private String detalles;
    private String pago;
    private Direccion PDireccion;

    /**
     * se genara el constructor vacio
     * **/
    public Pedido() {
    }
    /**
     * se genara el constructor con los parametros
     * **/
    public Pedido(int numero, LocalDate fechaCreacion, String datos, String detalles, String pago, Direccion PDireccion) {
        this.numero = numero;
        this.fechaCreacion = fechaCreacion;
        Datos = datos;
        this.detalles = detalles;
        this.pago = pago;
        this.PDireccion = PDireccion;
    }
    /**
     * se genaran los getters y setters
     * **/
    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public LocalDate getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(LocalDate fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getDatos() {
        return Datos;
    }

    public void setDatos(String datos) {
        Datos = datos;
    }

    public String getDetalles() {
        return detalles;
    }

    public void setDetalles(String detalles) {
        this.detalles = detalles;
    }

    public String getPago() {
        return pago;
    }

    public void setPago(String pago) {
        this.pago = pago;
    }

    public Direccion getPDireccion() {
        return PDireccion;
    }

    public void setPDireccion(Direccion PDireccion) {
        this.PDireccion = PDireccion;
    }
    /**
     * se genara el to string
     * **/
    @Override
    public String toString() {
        return "Pedidos{" +
                "numero='" + numero + '\'' +
                ", fechaCreacion=" + fechaCreacion +
                ", Datos='" + Datos + '\'' +
                ", detalles='" + detalles + '\'' +
                ", pago='" + pago + '\'' +
                ", PDireccion=" + PDireccion +
                '}';
    }

    /**
     * se genara el metedo equals
     * **/
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pedido pedido = (Pedido) o;
        return numero == pedido.numero && Objects.equals(fechaCreacion, pedido.fechaCreacion) && Objects.equals(Datos, pedido.Datos) && Objects.equals(detalles, pedido.detalles) && Objects.equals(pago, pedido.pago) && Objects.equals(PDireccion, pedido.PDireccion);
    }

}
