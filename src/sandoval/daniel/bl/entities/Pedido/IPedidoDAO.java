package sandoval.daniel.bl.entities.Pedido;


import java.util.ArrayList;

public interface IPedidoDAO {
    String registrarPedido(Pedido pedido) throws Exception;
    String actualizarPedido(Pedido pedido) throws Exception;
    String eliminarPedido(Pedido pedido) throws Exception;
    ArrayList<Pedido> listarPedido() throws Exception;
}
