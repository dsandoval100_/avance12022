package sandoval.daniel.bl.logic;


import sandoval.daniel.bl.entities.Moto.IMotoDAO;
import sandoval.daniel.bl.entities.Moto.Moto;
import sandoval.daniel.bl.entities.Moto.MySqlMotoImpl;

import java.util.ArrayList;

public class MotoGestor {

    private IMotoDAO motoDAO;

    public MotoGestor(){
        motoDAO = new MySqlMotoImpl();
    }



    public ArrayList<Moto> listarMoto() throws Exception{
        return motoDAO.listarMoto();
    }

    public Moto buscarMoto( String placa) throws Exception{
        Moto moto = new Moto();
        moto.setPlaca(placa);
        return motoDAO.buscarMoto(moto);
    }

    public String registrarMoto(String marca, String placa,String tipo) throws Exception{
        Moto moto = new Moto(marca,placa,tipo);
        return motoDAO.registrarMoto(moto);
    }

    public String actualizarMoto(String marca, String placa, String tipo) throws Exception{
        Moto moto = new Moto(marca,placa,tipo);
        return motoDAO.actualizarMoto(moto);
    }

    public String eliminarMoto(String placa) throws Exception{
        Moto moto = new Moto();
        moto.setPlaca(placa);
        return motoDAO.eliminarMoto(moto);
    }

}
