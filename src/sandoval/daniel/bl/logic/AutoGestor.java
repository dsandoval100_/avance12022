package sandoval.daniel.bl.logic;

import sandoval.daniel.bl.entities.Auto.Auto;
import sandoval.daniel.bl.entities.Auto.IAutoDAO;
import sandoval.daniel.bl.entities.Auto.MySqlAutoImpl;
import sandoval.daniel.bl.entities.Moto.IMotoDAO;
import sandoval.daniel.bl.entities.Moto.Moto;
import sandoval.daniel.bl.entities.Moto.MySqlMotoImpl;

import java.util.ArrayList;

public class AutoGestor {

    private IAutoDAO autoDAO;

    public AutoGestor(){
        autoDAO = new MySqlAutoImpl();
    }



    public ArrayList<Auto> listarAuto() throws Exception{
        return autoDAO.listarAuto();
    }

    public Auto buscarAuto( String placa) throws Exception{
        Auto auto = new Auto();
        auto.setPlaca(placa);
        return autoDAO.buscarAuto(auto);
    }

    public String registrarAuto(String marca, String placa,String tipo,String color, String anio) throws Exception{
        Auto auto = new Auto(marca,placa,tipo,color, anio);
        return autoDAO.registrarAuto(auto);
    }

    public String actualizarAuto(String marca, String placa, String tipo,String color, String anio) throws Exception{
        Auto auto = new Auto(marca,placa,tipo,color,anio);
        return autoDAO.actualizarAuto(auto);
    }

    public String eliminarAuto(String placa) throws Exception{
        Auto auto = new Auto();
        auto.setPlaca(placa);
        return autoDAO.eliminarAuto(auto);
    }

}
