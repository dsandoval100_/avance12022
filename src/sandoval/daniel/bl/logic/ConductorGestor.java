package sandoval.daniel.bl.logic;

import sandoval.daniel.bl.entities.Administrador.Administrador;
import sandoval.daniel.bl.entities.Conductor.Conductor;
import sandoval.daniel.bl.entities.Conductor.IConductorDAO;
import sandoval.daniel.bl.entities.Conductor.MySqlConductorImpl;

import java.time.LocalDate;
import java.util.ArrayList;

/**
 * se genara el gestor adminsitrador con su respectivo listar, registrar, eliminar y actualizar
 * **/

public class ConductorGestor {

    private IConductorDAO conductorDAO;

    public ConductorGestor(){
        conductorDAO = new MySqlConductorImpl();
    }

    public ArrayList<Conductor> listarConductor() throws Exception{
        return conductorDAO.listarConductor();
    }

    public Conductor buscarConductor(String usuario, String clave) throws Exception{
        Conductor conductor = new Conductor();
        conductor.setUsuario(usuario);
        conductor.setClave(clave);
        return conductorDAO.buscarConductor(conductor);
    }

    public String registrarConductor(String nombre, String apellido, int id, String usuario, String clave, String direccionExacta,String provincia, String canton, String distrito,String tipoPersona, String avatar, LocalDate fecha, int edad, String estado) throws Exception{
        Conductor conductor = new Conductor(nombre,apellido,id, usuario,clave,direccionExacta,provincia,canton,distrito,"Conductor",avatar,fecha,edad,estado);
        return conductorDAO.registrarConductor(conductor);
    }

    public String actualizarConductor(String nombre, String apellido, int id, String usuario, String clave, String direccionExacta,String provincia, String canton, String distrito,String tipoPersona,String avatar, LocalDate fecha, int edad, String estado) throws Exception{
        Conductor conductor = new Conductor(nombre,apellido,id, usuario,clave,direccionExacta,provincia,canton,distrito,"Conductor",avatar,fecha,edad,estado);
        return conductorDAO.actualizarConductor(conductor);
    }

    public String eliminarConductor(int id) throws Exception{
        Conductor conductor = new Conductor();
        conductor.setId(id);
        return conductorDAO.eliminarConductor(conductor);
    }

}
