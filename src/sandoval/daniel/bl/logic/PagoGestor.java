package sandoval.daniel.bl.logic;

import sandoval.daniel.bl.entities.Administrador.Administrador;
import sandoval.daniel.bl.entities.Administrador.IAdministradorDAO;
import sandoval.daniel.bl.entities.Administrador.MySqlAdministradorImpl;
import sandoval.daniel.bl.entities.Pago.IPagoDAO;
import sandoval.daniel.bl.entities.Pago.MySqlPagoImpl;
import sandoval.daniel.bl.entities.Pago.Pago;

import java.util.ArrayList;

public class PagoGestor {


    private IPagoDAO pagoDAO;

    public PagoGestor(){
        pagoDAO = new MySqlPagoImpl();
    }



    public ArrayList<Pago> listarPago() throws Exception{
        return pagoDAO.listarPago();
    }

    public String registrarPago(int numero, String proveedor,int id) throws Exception{
        Pago pago = new Pago(numero,proveedor,id);
        return pagoDAO.registrarPago(pago);
    }

    public String actualizarPago(int numero, String proveedor ,int id) throws Exception{
        Pago pago = new Pago(numero,proveedor,id);
        return pagoDAO.actualizarPago(pago);
    }

    public String eliminarPago(int numero) throws Exception{
        Pago pago = new Pago();
        pago.setNumero(numero);
        return pagoDAO.eliminarPago(pago);
    }

}
