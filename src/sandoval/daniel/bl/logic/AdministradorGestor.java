package sandoval.daniel.bl.logic;

import sandoval.daniel.bl.entities.Administrador.Administrador;
import sandoval.daniel.bl.entities.Administrador.IAdministradorDAO;
import sandoval.daniel.bl.entities.Administrador.MySqlAdministradorImpl;
import sandoval.daniel.bl.entities.Persona.Persona;
import sandoval.daniel.bl.entities.Persona.IPersonaDAO;
import sandoval.daniel.bl.entities.Persona.MySqlPersonaImpl;

import java.security.PublicKey;
import java.util.ArrayList;

/**
 * se genara el gestor adminsitrador con su respectivo listar, registrar, eliminar y actualizar
 * **/

public class AdministradorGestor {

    private IAdministradorDAO administradorDAO;

    public AdministradorGestor(){
        administradorDAO = new MySqlAdministradorImpl();
    }



    public ArrayList<Administrador> listarAdministradores() throws Exception{
        return administradorDAO.listarAdministradores();
    }

    public Administrador buscarAdministrador( String usuario, String clave) throws Exception{
        Administrador administrador = new Administrador();
        administrador.setUsuario(usuario);
        administrador.setClave(clave);
        return administradorDAO.buscarAdministrador(administrador);
    }

    public String registrarAdministrador(String nombre, String apellido, int id, String usuario, String clave, String direccionExacta,String provincia, String canton, String distrito,String tipoPersona) throws Exception{
        Administrador administrador = new Administrador(nombre,apellido,id,usuario,clave,direccionExacta,provincia,canton,distrito,"Administrador");
        return administradorDAO.registrarAdministrador(administrador);
    }

    public String actualizarAdministrador(String nombre, String apellido, int id, String usuario, String clave, String direccionExacta,String provincia, String canton, String distrito, String tipoPersona ) throws Exception{
        Administrador administrador = new Administrador(nombre,apellido,id,usuario,clave,direccionExacta,provincia,canton,distrito,"Administrador");
        return administradorDAO.actualizarAdministrador(administrador);
    }

    public String eliminarAdministrador(int id) throws Exception{
        Administrador administrador = new Administrador();
        administrador.setId(id);
        return administradorDAO.eliminarAdministrador(administrador);
    }


}
