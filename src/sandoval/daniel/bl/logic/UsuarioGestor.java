package sandoval.daniel.bl.logic;



import sandoval.daniel.bl.entities.Administrador.Administrador;
import sandoval.daniel.bl.entities.Usuario.IUsuarioDAO;
import sandoval.daniel.bl.entities.Usuario.MySqlUsuarioImpl;
import sandoval.daniel.bl.entities.Usuario.Usuario;

import java.time.LocalDate;
import java.util.ArrayList;

/**
 * se genara el gestor adminsitrador con su respectivo listar, registrar, eliminar y actualizar
 * **/
public class UsuarioGestor {

    private IUsuarioDAO usuarioDAO;

    public UsuarioGestor(){
        usuarioDAO = new MySqlUsuarioImpl();
    }

    public ArrayList<Usuario> listarUsuario() throws Exception{
        return usuarioDAO.listarUsuario();
    }

    public Usuario buscarUsuario(String usuario, String clave) throws Exception{
        Usuario usuario1 = new Usuario();
        usuario1.setUsuario(usuario);
        usuario1.setClave(clave);
        return usuarioDAO.buscarUsuario(usuario1);
    }

    public String registrarUsuario(String nombre, String apellido, int id, String usuario, String clave, String direccionExacta,String provincia, String canton, String distrito,String tipoPersona, String avatar, LocalDate fecha, int edad,String genero, int celular ) throws Exception{
        Usuario usuario1 = new Usuario(nombre,apellido,id, usuario,clave,direccionExacta,provincia,canton,distrito,"Usuario",avatar,fecha,edad,genero,celular);
        return usuarioDAO.registrarUsuario(usuario1);
    }

    public String actualizarUsuario(String nombre, String apellido, int id, String usuario, String clave, String direccionExacta, String provincia , String canton, String distrito,String tipoPersona,String avatar, LocalDate fecha, int edad, String genero, int celular) throws Exception{
        Usuario usuario1 = new Usuario(nombre,apellido,id, usuario,clave,direccionExacta,provincia,canton,distrito,"Usuario",avatar,fecha,edad,genero,celular);
        return usuarioDAO.actualizarUsuario(usuario1);
    }

    public String eliminarUsuario(int id) throws Exception{
        Usuario usuario = new Usuario();
        usuario.setId(id);
        return usuarioDAO.eliminarUsuario(usuario);
    }
}
