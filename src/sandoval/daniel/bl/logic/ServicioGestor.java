package sandoval.daniel.bl.logic;

import sandoval.daniel.bl.entities.Servicio.IServicioDAO;
import sandoval.daniel.bl.entities.Servicio.MySqlServicioImpl;
import sandoval.daniel.bl.entities.Servicio.Servicio;

import java.util.ArrayList;

public class ServicioGestor {

    private IServicioDAO servicioDAO;

    public ServicioGestor(){
        servicioDAO = new MySqlServicioImpl();
    }



    public ArrayList<Servicio> listarServicio() throws Exception{
        return servicioDAO.listarServicio();
    }

    public Servicio buscarServicio( int codigo) throws Exception{
        Servicio servicio = new Servicio();
        servicio.setCodigo(codigo);
        return servicioDAO.buscarServicio(servicio);
    }

    public String registrarServicio(int codigo, String descripcion, String precio, String tipo, String estado) throws Exception{
        Servicio servicio = new Servicio(codigo,descripcion,precio,tipo,estado);
        return servicioDAO.registrarServicio(servicio);
    }

    public String actualizarServicio(int codigo, String descripcion, String precio, String tipo, String estado) throws Exception{
        Servicio servicio = new Servicio(codigo,descripcion,precio,tipo,estado);
        return servicioDAO.actualizarServicio(servicio);
    }


    public String eliminarServicio(int codigo) throws Exception{
        Servicio servicio = new Servicio();
        servicio.setCodigo(codigo);
        return servicioDAO.eliminarServicio(servicio);
    }

}
