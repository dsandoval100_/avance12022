package sandoval.daniel.bl.logic;

import sandoval.daniel.bl.entities.Moto.IMotoDAO;
import sandoval.daniel.bl.entities.Moto.MySqlMotoImpl;
import sandoval.daniel.bl.entities.Persona.IPersonaDAO;
import sandoval.daniel.bl.entities.Persona.MySqlPersonaImpl;
import sandoval.daniel.bl.entities.Persona.Persona;
import sandoval.daniel.dao.DaoFactory;

import java.util.ArrayList;

public class PersonaGestor {

    private IPersonaDAO personaDAO;


    public PersonaGestor(){
        personaDAO = new MySqlPersonaImpl();
    }


    /**
     * Método para verificar que el usuario buscado existe, login
     * @param usuarioBuscado Persona
     * @return Persona Persona
     * @throws Exception
     */
    public Persona verificarAcceso(Persona usuarioBuscado) throws Exception {
        Persona Persona;
        Persona = personaDAO.verificarAcceso(usuarioBuscado);
        return Persona;
    }


    public ArrayList<Persona> verificarAdministrador() throws Exception {
        ArrayList<Persona> Persona = null;

        Persona = personaDAO.listarAdministradrores();
        return Persona;
    }

    public ArrayList<Persona> verificarConductor() throws Exception {
        ArrayList<Persona> Persona;
        Persona = personaDAO.listarConductores();
        return Persona;
    }


    public ArrayList<Persona> verificarUsuario() throws Exception {
        ArrayList<Persona> Persona;
        Persona = personaDAO.listarUsuarios();
        return Persona;
    }


   public ArrayList<Persona> consultarAdministrador(int idAdmin)throws Exception{
        ArrayList<Persona> Persona = null;
        ArrayList<Persona> admin =  new ArrayList<>();
        Persona = personaDAO.listarAdministradrores();
        int largo = Persona.size();
        //int idAdmin = 1234234;//
        for (int i=0;i<largo;i++){
            if (Persona.get(i).getId() == idAdmin){
                admin.add(Persona.get(i));
                return admin;
            }

        }
        return admin;
    }




}
